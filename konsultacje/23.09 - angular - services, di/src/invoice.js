
angular.module('Invoice', [])


angular.module('Invoice')
  .service('InvoiceService', function (
    $controller
  ) {
    // $controller('placki', { $scope: {} })

    this._invoice = {
      client: null,
      positions: [
        {
          id: "123",
          title: 'Pozycja 123',
          netto: 1000,
          tax: 23,
          brutto: 1230
        },
        {
          id: "234",
          title: 'Pozycja 234',
          netto: 2000,
          tax: 23,
          brutto: 2460
        },
      ],
      summary: {
        total: 2460
      }
    }

    this.getInvoice = () => this._invoice

    this.setClient = (client) => this._invoice.client = client

    this.addPosition = (index) => {
      this._invoice.positions.splice(index + 1, 0, {
        id: Date.now().toString()
      })
    }
    this.removePosition = (index) => {
      this._invoice.positions.splice(index, 1)
    }

  })
