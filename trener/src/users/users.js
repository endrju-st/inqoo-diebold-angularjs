
const users = angular.module('users', ['users.service'])

angular.module('users')
  .controller('UsersPageCtrl', ($scope, UsersService) => {
    const vm = $scope.usersPage = {}

    vm.users = UsersService.getUsers()
    vm.selected = null
    vm.draft

    vm.select = (id) => {
      vm.selected = UsersService.getUserById(id)
      vm.mode = 'details'
    }
    vm.edit = () => {
      vm.mode = 'edit'
      vm.draft = { ...vm.selected }
    }
    vm.save = (draft) => {
      const saved = UsersService.updateUser(draft)
      vm.users = UsersService.getUsers() 
      vm.select(saved.id)
    }
  })
  // .controller('UserListCtrl', ($scope) => {
  //   const vm = $scope.list = {}
  //   vm.filtered = []
  //   vm.select = (id) => $scope.$emit('selectUser', id)

  // })
  // .controller('UserDetailsCtrl', ($scope) => {
  //   const vm = $scope.details = {}
  //   vm.user = null

  //   $scope.$on('userSelected', (event, user) => { vm.user = (user) })

  // })
  // .controller('UserEditFormCtrl', ($scope) => {
  //   const vm = $scope.editform = {}
  //   vm.draft = {}
  // })
