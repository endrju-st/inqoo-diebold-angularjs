
angular.module('users.service', [])
  .service('UsersService', function () {
    console.log('Hello UsersService');

    this._users = [
      { id: "123", username: 'Admin' }
    ];

    this.fetchUsers = () => {
      'http://localhost:3000/users'
    }
    this.fetchUsers()

    this.getUsers = () => { return this._users; };
    this.updateUser = (draft) => {
      this._users = this._users.map(u => u.id === draft.id ? draft : u)
      return draft
    };
    this.getUserById = (id) => { return this._users.find(u => u.id == id); };

  });
