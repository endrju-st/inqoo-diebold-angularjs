
const tasks = angular.module('tasks', [])

tasks.constant('INITIAL_TASKS', [])
tasks.constant('INITIAL_MODE', 'show')
tasks.controller('TasksCtrl', (
  $scope,
  INITIAL_TASKS,
  INITIAL_MODE) => {

  console.log('Hello TasksCtrl', { ...$scope })

  $scope.mode = INITIAL_MODE
  $scope.tasks = INITIAL_TASKS
  $scope.filtered = []

  $scope.draft = {}

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true
  }


  $scope.query = ''
  $scope.changeQuery = () => {
    $scope.filtered = $scope.tasks.filter(
      t => t.name.toLocaleLowerCase().includes($scope.query.toLocaleLowerCase())
    )
  }

  $scope.counts = { completed: 0, total: 0 }
  $scope.updateCounts = () => {
    $scope.counts = $scope.tasks.reduce((counts, task) => {
      counts.total += 1;
      if (task.completed) { counts.completed += 1 }
      return counts
    }, { completed: 0, total: 0 })
  }

  // shallow compares immutable values ( === )
  $scope.$watch('tasks', (newVal, oldVal) => {
    // $scope.$watchCollection('tasks', (newVal, oldVal) => {
    // debugger
    console.log(' task watcher update');
    $scope.changeQuery('')
    $scope.updateCounts()
  })
  // $scope.changeQuery('')

  $scope.edit = () => {
    $scope.mode = 'edit'
    // $scope.draft = $scope.selectedTask
    $scope.draft = { ...$scope.selectedTask }
  }

  $scope.cancel = () => {
    $scope.mode = 'show'
  }

  $scope.toggleCompleted = (draft) => {
    draft.completed = !draft.completed
    $scope.tasks = $scope.tasks.map(
      task => task.id == draft.id ? draft : task
    )
  }

  $scope.save = () => {
    $scope.mode = 'show'
    $scope.selectedTask = { ...$scope.draft }
    $scope.tasks = $scope.tasks.map(
      task => task.id == $scope.draft.id ? $scope.draft : task
    )
    // $scope.changeQuery($scope.query)
  }

  $scope.addTask = () => {
    // $scope.tasks.push({ // mutable wont trigger $watch!
    $scope.tasks = [...$scope.tasks, { // Immutable copy // can use simple $watch
      id: Date.now().toString(),
      name: $scope.fastTaskName, completed: false
    }]
    $scope.fastTaskName = ''
    // $scope.changeQuery($scope.query)
  }

  $scope.remove = (id) => {
    $scope.tasks = $scope.tasks.filter(task => task.id !== id)
    // $scope.changeQuery($scope.query)
  }

  $scope.select = (item) => {
    $scope.selectedTask = item
  }
})