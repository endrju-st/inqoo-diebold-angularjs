const installementService = angular.module('installementService', [])

installementService.service(InstallementService, function () {

    this.calculateInstallement = (months, amount, interest) => {
        let restOfLoan = amount
        let installement = 0
        let capitalPart = (amount / months)
        let listOfInstallements = []
        for (let index = 0; index < months; index++) {
            installement = capitalPart +
                (restOfLoan * interest) / 100 / months;
            restOfLoan = restOfLoan - capitalPart;
            listOfInstallements[index] = installement;
        }
        return listOfInstallements;
    }
}
)