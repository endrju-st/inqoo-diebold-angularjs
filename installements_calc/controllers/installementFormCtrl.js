const installementCtrl = angular.module("installementCtrl", [])

installementCtrl.controller("InstallementCtrl", function ($scope, InstallementService) {
    const vm = $scope.InstallementService

    vm.loan = { amount: 0, months: 6, interest: 1 }
    InstallementService.calculateInstallement()
})