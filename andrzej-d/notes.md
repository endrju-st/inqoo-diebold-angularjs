const users = angular.module("users", ["usersService"]);

users
  .controller("UsersPageCtrl", ($scope, UsersService) => {
    const vm = ($scope.usersPage = {});

    vm.users = UsersService.getUsers();

    vm.select = (param) => {
      vm.selectedUser = UsersService.getUsersById(param);
      vm.mode = "editUser";
    };

    vm.save = (user) => {
      UsersService.updateUser(user);
      console.log("object");
      vm.mode = "saved"
    };
    
    
  })
  .controller("UsersListCtrl", ($scope) => {
    const vm = ($scope.list = {});
  })
  .controller("UsersDetails", ($scope) => {
    const vm = ($scope.details = {});
  })
  .controller("UsersEditFormCtrl", ($scope) => {
    const vm = ($scope.editForm = {});

    
    vm.initForm = (selectedUser) => {
        console.log(selectedUser);
        vm.userTmp={...selectedUser};
    }
  });



-----------------------------------------------

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link
      rel="stylesheet"
      href="node_modules/bootstrap/dist/css/bootstrap.css"
    />
    <link rel="stylesheet" href="node_modules/angular/angular-csp.css" />

    <script src="node_modules/angular/angular.js" defer></script>
    <script src="src/tasks/tasks.js" defer></script>
    <script src="src/users/userService.js" defer></script>
    <script src="src/users/users.js" defer></script>
    <script src="src/index.js" defer></script>
  </head>

  <body ng-controller="AppCtrl">
    <div class="container">
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div class="container">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item"ng-click="changeViewMode('tasks')">
                <a class="nav-link" href="#" >Tasks</a>
              </li>
              <li class="nav-item" ng-click="changeViewMode('users')">
                <a class="nav-link" href="#" >Users</a>
              </li>
            </ul>
            <div class="ms-auto navbar-text">
              <span>Welcome {{ user.name }}, Login </span>
            </div>
          </div>
        </div>
      </nav>
      <div class="container" ng-controller="UsersPageCtrl" ng-If="viewMode === 'users'">
        <div class="row">
          <div class="col-5" ng-controller="UsersListCtrl">
            <h4>Users List</h4>
            <div class="list-group" ng-repeat="user in usersPage.users" ng-click="usersPage.select(user.id)">
              <div class="list-group-item">{{user.username}}</div>            
            </div>
          </div>
          <div class="col-5" ng-controller="UsersDetails">
            <h4>Selected user details</h4>
            <dl>
              <dt>{{usersPage.selectedUser.id}}</dt>
              <dd>{{usersPage.selectedUser.username}}</dd>
            </dl>
          </div>
        </div>
        <hr>
        <form ng-if="usersPage.mode === 'editUser' && usersPage.selectedUser" ng-controller="UsersEditFormCtrl"
        ng-init="editForm.initForm(usersPage.selectedUser)">
          <!-- .form-group>label{Name}+input.form-control -->
          
          <div class="form-group mb-3"><label for="task_name">Username</label>
            <input id="task_name" type="text" class="form-control" ng-model="editForm.userTmp.username" >
          </div>
         
          <button class="btn btn-info" ng-click="usersPage.save(editForm.userTmp)">Save</button>
          <!-- <button class="btn btn-info" ng-click="addTask()"ng-show="mode === 'add'"ng-hide="mode === 'hide'||mode ==='edit'">Save</button> -->
        </form>
      </div>
    
      <div class="row">
        <!-- <div class="col">
          <h1>Angular {{title}}</h1> 
          <div>
            <div>
              <span>Welcome {{ user.name }}</span>
            </div>
            <span ng-click="login()" ng-hide="isLoggedIn"> Login </span>
            <span ng-click="logout() " ng-show="isLoggedIn"> Logout </span>
            <input type="text" ng-model="user.name" />
          </div>
        </div> -->
        <div class="row" ng-controller="TasksCtrl" ng-If="viewMode === 'tasks'">
          <div class="input-group" >
            <span class="input-group-text" id="basic-addon1">Filter by Name</span>
            <input type="text" class="form-control" placeholder="Filter by Name" aria-describedby="basic-addon1" ng-change="filter()"  ng-model="filterName">
          </div>
          <div class="list-group ms-3">
            <div class="list-group-item d-flex justify-content-between " ng-click="selectTask(item.id)" 
             ng-class="{
              'active' : task.id === item.id,
              'text-decoration-line-through' : item.completed === true
            }" 
            ng-repeat="item in filteredTasks track by item.id">{{$index+1}}.{{item.name}}<button class="btn-close" ng-click="remove(item.id)"></button></div>
          
          </div>
          <ul class="list-group ms-3">
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Completed Tasks
              <span class="badge bg-primary rounded-pill">{{completed}}</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Uncompleted Tasks
              <span class="badge bg-primary rounded-pill">{{uncompleted}}</span>
            </li>
            </ul>
          
          <div class="col">
    
          </div>
            <dl>
              <dt>Name:</dt>
              <dd>{{task.name}}</dd>
              <dt>Completed:</dd>
              <dd>{{task.completed?"yes":"no"}}</dd>
              <button class="btn btn-info" ng-click="edit()">Edit</button>      
              <button class="btn btn-info" ng-click="cancel()">Cancel</button>
              <button class="btn btn-warning" ng-click="add()">Add</button>
              <form ng-show="mode === 'edit' || mode === 'add'" ng-keypress="enterPress($event)">
                <!-- .form-group>label{Name}+input.form-control -->
                <div class="form-group mb-3"><label for="task_name">Name</label>
                  <input id="task_name" type="text" class="form-control" ng-model="taskTmp.name" >
                </div>
                <div class="form-group mb-3"><label for="task_completed">
                    <input id="task_completed" type="checkbox" ng-model="taskTmp.completed">
                    Completed</label>
                </div>
                <!-- <button class="btn btn-info" ng-click="save()"ng-show="mode === 'edit'"ng-hide="mode === 'hide'||mode === 'add'">Save</button> -->
                <!-- <button class="btn btn-info" ng-click="addTask()"ng-show="mode === 'add'"ng-hide="mode === 'hide'||mode ==='edit'">Save</button> -->
              </form>
            </dl>
          </div>
          <div class="col"></div>
        </div>
      </div>
    </div>
  </body>
</html>
