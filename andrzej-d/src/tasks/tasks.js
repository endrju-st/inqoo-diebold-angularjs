const tasks = angular.module("tasks", []);
tasks.controller("TasksCtrl", ($scope) => {
  $scope.task = {
    id: "123",
    name: "Task 123 ",
    completed: true,
  };
  $scope.tasks = [
    {
      id: "123",
      name: "Task 123",
      completed: true,
    },
    {
      id: "234",
      name: "Task 234",
      completed: false,
    },
    {
      id: "345",
      name: "Task 345",
      completed: true,
    },
  ];

  $scope.filteredTasks = [...$scope.tasks];
  $scope.filterName='';




  $scope.selectTask = (itemId) => {
    $scope.task = $scope.tasks.find((elem) => elem.id === itemId);
  };

  $scope.edit = () => {
    $scope.mode = "edit";
    $scope.taskTmp = { ...$scope.task };
  };

  $scope.save = () => {
    $scope.task = { ...$scope.taskTmp };
    $scope.tasks = $scope.tasks.map((elem) =>
      elem.id === $scope.taskTmp.id ? $scope.taskTmp : elem
    );
    $scope.filter();
  };

  $scope.cancel = () => {
    $scope.mode = "hide";
  };

  $scope.remove = (itemId) => {
    $scope.tasks = $scope.tasks.filter((elem) => elem.id !== itemId);
    $scope.task = {};
    $scope.filter();
    // $scope.count();
  };

  $scope.add = () => {
    $scope.mode = "add";
    $scope.taskTmp = {name:""};
  };
  $scope.addTask = () => {
    $scope.mode = "add";
    $scope.taskTmp.id = Math.random() * 100 + "";   
    $scope.tasks.push($scope.taskTmp);
   $scope.add();
    $scope.filter();
    // $scope.count();
  };

  $scope.filter = () => {
    $scope.filteredTasks = $scope.tasks.filter((item) =>
      item.name.toLowerCase().includes($scope.filterName.toLowerCase())
    );
//    $scope.count();
  };

  $scope.enterPress = (e) => {
    if (e.which === 13 && $scope.mode === "add") {
      $scope.addTask();
      
    }
    if (e.which === 13 && $scope.mode === "edit") {
      $scope.save();
      $scope.cancel();
    }
  };

  $scope.count = () => {
    $scope.completed=0;
    $scope.uncompleted=0;
      $scope.tasks.forEach(element => {
          if(element.completed){
              $scope.completed++;
          } else {
            $scope.uncompleted++;
          }          
      });
  }
  $scope.count();

  $scope.$watchCollection('tasks',(oldVal,newVal) => {
      $scope.count()
  })
});
