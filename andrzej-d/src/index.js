
// console.log('Hello Angular');
// console.log(angular.version);


angular.module('config',[]).constant('PAGES', [
    { name: 'users', label: 'Users' },
  ])
angular.module('myApp',['config','tasks','users','settings'])
const app = angular.module('myApp').constant('PAGES', [
    { name: 'tasks', label: 'Tasks', tpl:'src/views/tasks-view.html' },
    { name: 'users', label: 'Users', tpl:'src/views/user-view.html' },
    { name: 'setting', label: 'Settings', tpl:'src/views/setting-view.html' }
  ])

//Global scope
app.run(function($rootScope,PAGES) {
    $rootScope .title = 'myApp'
})
//local scope
app.controller('AppCtrl',($scope,PAGES)=> {
    $scope.user = { name: 'Guest'}
    $scope.isLoggedIn = false;

    $scope.login = () => {
        $scope.user.name = 'Admin'
        $scope.isLoggedIn = true
    }
    $scope.logout = () => {
        $scope.user.name = 'Guest'
        $scope.isLoggedIn = false
    }
    $scope.changeViewMode = (par) =>{
                $scope.viewMode=par
        
    }
    $scope.pages = PAGES

    $scope.currentPage = $scope.pages[1]
  
    $scope.goToPage = pageName => {
      $scope.currentPage = $scope.pages.find(p => p.name === pageName)
    }
})

angular.bootstrap(document,['myApp'])
