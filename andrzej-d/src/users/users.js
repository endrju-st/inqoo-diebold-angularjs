const users = angular.module("users", ["usersService"]);

users.controller("UsersPageCtrl", ($scope, UsersService, $http) => {
  const vm = ($scope.usersPage = {});
  // vm.users = UsersService.getUsers()
  vm.selectedUser = null;
  vm.userTmp;

  vm.select = (user) => {
    UsersService.getUsersById(user).then((resp) => {
      vm.selectedUser = resp.data;
      vm.userTmp = { ...vm.selectedUser };
    });
    vm.mode = "editUser";
  };
  vm.edit = () => {
    vm.mode = "editUser";
    vm.userTmp = { ...vm.selectedUser };
  };

  vm.save = (draft) => {
    UsersService.updateUser(draft).then(() => {
      vm.refresh();
    });
    // vm.select(saved.id)
  };

  vm.add = () => {
    vm.mode = "addUser";
    vm.newUser = {};
  };

  vm.addNewUser = (user) => {
    UsersService.addUser(user).then(() => {
      vm.refresh();
    });
  };

  vm.refresh = () => {
    UsersService.fetchUsers().then((data) => {
      vm.users = data;
    });
  };
  vm.delete = (user) => {
    UsersService.deleteUser(user).then(() => {
      vm.refresh();
    });
  };
  
  vm.refresh();
});
