const usersService = angular.module("usersService", []);

usersService.service("UsersService", function ($http) {
  console.log("Hello UserService");
  this._users = [
    {
      id: "123",
      username: "Admin",
    },
    {
      id: "234",
      username: "User1",
    },
    {
      id: "345",
      username: "User2",
    },
  ];
  this.getUsers = () => {
    return [...this._users];
  };
  this.fetchUsers = () => {
    return $http.get("http://localhost:3000/users").then((res) => {
      return (this._users = res.data);
    });
  };

  // this.fetchUsers = () => {
  //   fetch("http://localhost:3000/users")
  //     .then((resp) => resp.json())
  //     .then((resp) => {
  //       this._users = resp;
  //     });
  // };
  // this.fetchUsers()

  this.getUsersById = (user) => {
    return $http.get("http://localhost:3000/users/"+ user.id)
    // return this._users.find((elem) => elem.id == userId);
  };

  this.updateUser = (userTmp) => {    
    return $http.put(("http://localhost:3000/users/"+ userTmp.id), userTmp)
    // this._users = this._users.map((elem) =>
    //   elem.id === userTmp.id ? userTmp : elem
    // );
  };

  this.addUser = (newUser) => {
    return $http.post("http://localhost:3000/users/",newUser)
  }

  this.deleteUser = (user) => {
     return $http.delete("http://localhost:3000/users/"+ user.id)
  }
});
